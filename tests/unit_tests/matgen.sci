function mat = matgen( siz );
//
// matrix generator function for templates tester
//

[lhs,rhs]=argn(0);

if( rhs== 0 ),
   error("Scalar is expected");
end

if ( type(siz) ~= 1 ),
   error("Scalar is expected");
end

if ( size(siz,1) ~= 1 | size(siz,2) ~= 1 ),
   error("siz must be a scalar");
end

if ( rhs > 1 ),
   error("Too input arguments");
end

// begin of computations

   if ( siz == 10 ),
      mat = makefish( 4 );           // poisson matrix
   elseif ( siz == 20  ),
      mat = wathen( 3, 3, 0 );       // spd consistent mass matrix
   elseif ( siz == 30  ),
      mat = wathen( 3, 3, 1 );       // spd consistent mass matrix
   elseif ( siz == 40 ),
      mat = lehmer(5);
   elseif ( siz == 50 ),
      mat = lehmer(10);
   else
      mat = makefish( 4 );           // irrelevant: x0 = exact solution
   end

//END matgen.sci
