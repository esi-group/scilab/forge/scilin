mode (-1);
getf("matgen.sci");
// function[ test_it ] = tester()
//
// templatestester loops over the various MatLab versions of the iterative
// templates. Test matrices are generated in matgen.m. Results are printed 
// to the screen.

//  INITIALIZATION

no_soln_jac   = 0;                     // convergence check
no_soln_sor   = 0;
no_soln_pcg    = 0;
no_soln_cheby = 0;
no_soln_gmres = 0;
no_soln_bicg  = 0;
no_soln_cgs   = 0;
no_soln_bicgs = 0;
no_soln_qmr   = 0;

guess_err_jac   = 0;                   // initial guess = solution error
guess_err_sor   = 0;                   
guess_err_pcg    = 0;                   
guess_err_cheby = 0;                   
guess_err_gmres = 0;                   
guess_err_bicg  = 0;                   
guess_err_cgs   = 0;                   
guess_err_bicgs = 0;                   
guess_err_qmr   = 0;                   

allpassed = 0;

num_tests = 6;
ep = %eps;
tol = ep * 1000;

//  BEGIN TESTING: FORM SYSTEM AND APPLY ALGORITHMS

for test = 1:num_tests
  test
  A = matgen( test*10 );             // form test matrix
  [sizeA,sizeA] = size(A);
  max_it = sizeA * 10;
  normA = norm( A,%inf );
  if ( test == 1 | test == 2 | test == 3 | test == 6 ),
    for i = 1:sizeA,                // set rhs = row sums
      temp = 0.0;
      for j = 1:sizeA,
	temp = temp + A(i,j);
      end
      b(i,1) = temp;
    end
  else 
    b = ones(sizeA,1);              // set rhs = unit vector
  end
  if ( test < 4 ),
    M = eye(sizeA,sizeA);                 // no preconditioning
  else
    M = diag(diag(A));              // diagonal preconditioning
  end
  
  if ( test < 6 ),
    xk = 0*b;                       // initial guess = zero vector
  else
    xk = ones(sizeA,1 );            // initial guess = solution
  end

  //     TEST EACH METHOD; CHECK ACCURACY IF CONVERGENCE CLAIMED

  //     Test Jacobi and Chebyshev

  if ( test == 1 | test == 6 ),
    jac_maxit = max_it * 3;
    [x, err, iter, flag_jac] = jacobi(A, b, xk, jac_maxit, tol);
    if ( flag_jac ~= 0 & test ~= 6 ),
      no_soln_jac = no_soln_jac + 1;
      printf("jacobi failed to converge for");
      test, err
      allpassed = allpassed + 1;
    end
    if ( test == 6 & iter ~= 0 & flag_jac ~= 0 )
      guess_err_jac = guess_err_jac + 1;
      printf("jacobi failed for initial guess = solution");
      test, iter, flag_jac
      allpassed = allpassed + 1;
    end
    
    [x, err, iter, flag_cheby] = cheby(A, b, xk, M, max_it, tol);
    if ( flag_cheby ~= 0 & test ~= 6 ),
      no_soln_cheby = no_soln_cheby + 1;
      printf("chebyshev failed to converge for");
      test, err
      allpassed = allpassed + 1;
    end
    if ( test == 6 & iter ~= 0 & flag_cheby ~= 0 )
      guess_err_cheby = guess_err_cheby + 1;
      printf("chebyshev failed for initial guess = solution");
      test, iter, flag_cheby
      allpassed = allpassed + 1;
    end
    
  end

  //     SPD ROUTINES

  if ( test == 1 | test == 2 | test == 4 | test == 6 ), 
    
    if ( test == 1 ),                     // various relaxation parameters
      w = 1.0;
    elseif ( test == 2 ),
      w = 1.2;
    else
      w = 1.1;
    end
    
    sor_maxit = max_it * 3;
    [x, err, iter, flag_sor] = sor(A, b, xk, w, sor_maxit, tol);
    if ( flag_sor ~= 0 & test ~= 6 ),
      no_soln_sor = no_soln_sor + 1;
      printf("sor failed to converge for");
      test, err
      allpassed = allpassed + 1;
    end
    if ( test == 6 & iter ~= 0 & flag_sor ~= 0 ),
      guess_err_sor = guess_err_sor + 1;
      printf("sor failed for initial guess = solution");
      test, iter, flag_sor
      allpassed = allpassed + 1;
    end
    
    [x, err, iter, flag_pcg] = pcg(A, b, xk, M, max_it, tol);
    if ( flag_pcg ~= 0 & test ~= 6 ),
      no_soln_pcg = no_soln_pcg + 1;
      printf("pcg failed to converge for");
      test, err
      allpassed = allpassed + 1;
    end
    if ( test == 6 & iter ~= 0 & flag_pcg ~= 0 ),
      guess_err_pcg = guess_err_pcg + 1;
      printf("pcg failed for initial guess = solution");
      test, iter, flag_pcg
      allpassed = allpassed + 1;
    end
  end

  //     Nonsymmetric ROUTINES

  if ( test == 1 | test == 4 | test == 5 | test == 6 ) 
    
    restrt = test*10;
    if ( restrt == 0 ),restrt = 1; end;
    [x, err, iter, flag_gmres]=gmres( A, b, xk, M, restrt, max_it, tol );
    if ( flag_gmres ~= 0 & test ~= 6 ),
      no_soln_gmres = no_soln_gmres + 1;
      printf("gmres failed to converge for");
      test
      allpassed = allpassed + 1;
    end
    if ( test == 6 & iter ~= 0 & flag_gmres ~= 0 )
      guess_err_gmres = guess_err_gmres + 1;
      printf("gmres failed for initial guess = solution");
      test, iter, flag_gmres
      allpassed = allpassed + 1;
    end
    [x, err, iter, flag_bicg] = bicg(A, b, xk, M, max_it, tol);
    if ( flag_bicg ~= 0 & test ~= 6 ),
      no_soln_bicg = no_soln_bicg + 1;
      printf("bicg failed to converge for");
      test, err
      allpassed = allpassed + 1;
    end
    if ( test == 6 & iter ~= 0 & flag_bicg ~= 0 )
      guess_err_bicg = guess_err_bicg + 1;
      printf("bicg failed for initial guess = solution");
      test, iter, flag_bicg
      allpassed = allpassed + 1;
    end
    
    [x, err, iter, flag_cgs] = cgs(A, b, xk, M, max_it, tol);
    if ( flag_cgs ~= 0 & test ~= 6 ),
      no_soln_cgs = no_soln_cgs + 1;
      printf("cgs failed to converge for");
      test, err
      allpassed = allpassed + 1;
    end
    if ( test == 6 & iter ~= 0 & flag_cgs ~= 0 )
      guess_err_cgs = guess_err_cgs + 1;
      printf("cgs failed for initial guess = solution");
      test, iter, flag_cgs
      allpassed = allpassed + 1;
    end
    
    [x, err, iter, flag_bicgs] = bicgstab(A, b, xk, M, max_it, tol);
    if ( flag_bicgs ~= 0 & test ~= 6 ),
      no_soln_bicgs = no_soln_bicgs + 1;
      printf("bicgstab failed to converge for");
      test, err
      allpassed = allpassed + 1;
    end
    if ( test == 6 & iter ~= 0 & flag_bicgs ~= 0 ),
      guess_err_bicgs = guess_err_bicgs + 1;
      printf("bicgstab failed for initial guess = solution");
      test, iter, flag_bicgs
      allpassed = allpassed + 1;
    end
    
    [x, err, iter, flag_qmr] = qmr(A, b, xk, M, M, max_it, tol);
    if ( flag_qmr ~= 0 & test ~= 6 ),
      no_soln_qmr = no_soln_qmr + 1;
      printf("qmr failed to converge for");
      test, err
      allpassed = allpassed + 1;
    end
    if ( test == 6 & iter ~= 0 & flag_qmr ~= 0 ),
      guess_err_qmr = guess_err_qmr + 1;
      printf("qmr failed for initial guess = solution");
      test, iter, flag_qmr
      allpassed = allpassed + 1;
    end
  end
end

//  REPORT RESULTS

TESTING = printf("             COMPLETE");

if ( allpassed == 0 ),
RESULTS = printf("             ALL TESTS PASSED");end

if ( no_soln_jac ~= 0 ),
  printf("jacobi failed test (failed to converge)");
elseif ( guess_err_jac ~= 0 ),
  printf("jacobi failed test (initial guess = solution error)");
else
  printf("jacobi passed test");
end

if ( no_soln_sor ~= 0 ),
  printf("sor failed test (failed to converge)");
elseif ( guess_err_sor ~= 0 ),
  printf("sor failed test (initial guess = solution error)");
else
  printf("sor passed test");
end

if ( no_soln_pcg ~= 0 ),
  printf("pcg failed test (failed to converge)");
elseif ( guess_err_pcg ~= 0 ),
  printf("pcg failed test (initial guess = solution error)");
else
  printf("pcg passed test");
end

if ( no_soln_cheby ~= 0 ),
  printf("cheby failed test (failed to converge)");
elseif ( guess_err_cheby ~= 0 ),
  printf("cheby failed test (initial guess = solution error)");
else
  printf("cheby passed test");
end

if ( no_soln_gmres ~= 0 ),
  printf("gmres failed test (failed to converge)");
elseif ( guess_err_gmres ~= 0 ),
  printf("gmres failed test (initial guess = solution error)");
else
  printf("gmres passed test");
end

if ( no_soln_bicg ~= 0 ),
  printf("bicg failed test (failed to converge)");
elseif ( guess_err_bicg ~= 0 ),
  printf("bicg failed test (initial guess = solution error)");
else
  printf("bicg passed test");
end

if ( no_soln_cgs ~= 0 ),
  printf("cgs failed test (failed to converge)");
elseif ( guess_err_cgs ~= 0 ),
  printf("cgs failed test (initial guess = solution error)");
else
  printf("cgs passed test");
end

if ( no_soln_bicgs ~= 0 ),
  printf("bicgstab failed test (failed to converge)");
elseif ( guess_err_bicgs ~= 0 ),
  printf("bicgstab failed test (initial guess = solution error)");
else
  printf("bicgstab passed test");;
end

if ( no_soln_qmr ~= 0 ),
  printf("qmr failed test (failed to converge)");
elseif ( guess_err_qmr ~= 0 ),
  printf("qmr failed test (initial guess = solution error)");
else
  printf("qmr passed test");
end

//  END TEMPLATES TESTING
