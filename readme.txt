Scilab Scilin Toolbox

Purpose
-------

Scilin is a Scilab toolbox for sparse linear systems. 
It provides preconditionners and iterative methods for the sparse linear equations.
Scilin contains:
 * Incomplete LU factorisations
 * Iterative solvers
 * Matrix Market I/O functions
 * Miscellaneous 

This project is OBSOLETE.

Please see the following projects:
 * imsls : Iterative Methods for Sparse Linear Equations
   http://forge.scilab.org/index.php/p/imsls/
 * matmark : Matrix Market
   http://forge.scilab.org/index.php/p/matmark/
 * spilu : Sparse Incomplete LU Preconditionners
   http://forge.scilab.org/index.php/p/spilu/


Features
--------

 * Iterative solvers
   * bicg: BIConjugate Gradient method
   * bicgstab: BIConjugate Gradient STABilized method
   * pcg: Conjugate Gradient method
   * cgs: Conjugate Gradient Squared method
   * cheby: CHEBYshev method
   * gmres: Generalized Minimal RESidual method
   * jacobi: JACOBI method
   * qmr: Quasi Minimal Residual method
   * sor: Successive Over-Relaxation method

 * Incomplete LU factorisation
   * ilut: Incomplete LU factorization with dual Truncation strategy
   * ilutp: ilut with column Pivoting
   * ilud: ilu with single dropping + Diagonal compensation ( ~milut )
   * iludp: ilud with column Pivoting
   * iluk: level-k ilu
   * ilu0: simple ilu(0) preconditioning
   * milu0: milu(0) preconditioning

 * Matrix Market I/O functions
   * mminfo: extracts storage information from a Matrix Market file (MM file)
   * mmread: reads contents of MM file into sparse or dense format
   * mmwrite: writes sparse or dense matrix to a MM file

 * Miscellaneous functions
   * det: has been extended to sparse format.
   * nnz: gives the number of non-nul elements of a sparse matrix


Contents
--------

* precond/:       This directory contains interfaces of robust 
                   preconditioner routines using dual thresholding
                   for dropping strategy. The routines come from 
                   the tool-box SPARSKIT.

* solvers/:       This directory contains several basic iterative linear system
                   solvers, written in Scilab language. The routines are adaptations
                   of the mltemplates library.

* man/:           This directory contains files for online help.

* matio/:         This directory contains some matrix generation routines from
                   SPARSKIT and some matrix market input/output routines, adapted
                   in Scilab language. 

* addon/:         This directory contains some useful functions, such as 
                   the determinant or the number of non null elements of a
                   sparse matrix. The function %sp_l_s.sci allow now a upper
                   and lower triangular resolution with optimized algorithms.
                                                           
* demos/:         This directory contains four demos using solvers 
                   and preconditioners with some typical matrices.
                   ( see the directory /demos/matrices )

History
-------

This module has been developed by Aladin Group (IRISA-INRIA) from a basic tool-kit
for sparse matrix computations, SPARSKIT (developed by Y.Saad,
University of Minnesota) and from the mltemplates library

Licence
-------

This toolbox is released under the terms of the CeCILL license :
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

Authors
-------

 * 1993 - Univ. of Tennessee and Oak Ridge National Laboratory
 * 2000 - 2001 - INRIA - Aladin Group
 * 2010 - DIGITEO - Michael Baudin

Bibliography
------------

http://www.irisa.fr/aladin/codes/SCILIN/
http://www.netlib.org/templates/
http://graal.ens-lyon.fr/~jylexcel/scilab-sparse/meeting07/

