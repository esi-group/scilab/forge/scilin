.TH iludp G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
iludp - ILUD with column pivoting
.SH CALLING SEQUENCE
.nf
[L,U,perm,ierr]=iludp(A,alph,drop,ptol,bloc)
.fi
.SH PARAMETERS
.TP
A
: sparse matrix (must be square)
.TP
alph
: the diagonal compensation parameter (default: 0.5)
.TP
drop
: threshold for dropping elements in \fBL\fP and \fBU\fP (default: 0.001*max(Aij))
.TP
ptol
: tolerance ratio used for determining whether to permute two columns (default: 0.5)
.TP
bloc
: permuting can be done within the diagonal blocks of size mbloc (default: n, with n the size of \fBA\fP)
.TP
L
: lower triangular sparse matrix
.TP
U
: upper triangular sparse matrix
.TP
perm
: contains the permutation arrays
.TP
ierr
: error message
.SH DESCRIPTION
Builds an incomplete LU factorization of the sparse matrix \fBA\fP. The two factors \fBL\fP and \fBU\fP are stored in the CSR format. The CSR format is the Compressed Sparse Row format used by Scilab. The matrix \fBA\fP must be square.
.TP 
alph
: The term \fBalph\fP*(sum of all dropped out elements in a given row) is added to the diagonal element of \fBU\fP of the factorization. Thus:
.LF
   alph = 0 --->ILU with threshold,
.LF
   alph = 1 --->MILU with threshold.
.TP
ptol
: Two columns are permuted only when abs(a(i,j))*\fBptol\fP > abs(a(i,i)) [0 --> never permute; recommended values from 0.1 to 0.01.
.TP
bloc
: Useful for PDE problems with several degrees of freedom. To discard the feature, set \fBbloc\fP=n.
.TP
ierr
: error flag.
.LP
   0  --> successful return.
.LP
  >0  --> zero pivot encountered at step number ierr.
.LP
  -1  --> input matrix may be wrong. (The elimination process has generated a row in \fBL\fP or \fBU\fP whose length is .gt.  n.)
.LP
  -2  --> storage of the \fBLU\fP factors caused an overflow in arrays alu/jlu.
.LP
  -3  --> zero row encountered.
.SH EXAMPLE
.nf
A=mmread(SCILIN+'/tests/matrices/nos1.mtx')
n=size(A,1); b=ones(n,1);
alph=0;drop=0;ptol=0;bloc=3;
[L,U,iperm,ierr]=iludp(A,alph,drop,ptol,bloc);
x=U\\(L\\b)
A*x-b
.fi
.SH AUTHOR
Sparskit procedure interfaced by Aladin Group
.SH SEE ALSO
ilu0, milu0, iluk, ilut, ilud, ilutp

