<?xml version="1.0" encoding="ISO-8859-1"?><refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="ilud"><info><pubdate>March 2000  </pubdate></info><refnamediv><refname>ilud</refname><refpurpose> sparse incomplete lu factorization with single dropping and diagonal compensation  </refpurpose></refnamediv>
  
   
  
   
  
   
  
   
  
   
  
   <refsynopsisdiv><title>Calling Sequence</title><synopsis>[L,U,ierr]=ilud(A,alph,drop)</synopsis></refsynopsisdiv>
  
   <refsection><title>Parameters</title>
 
      <variablelist>
  
         <varlistentry>
  
            <term>A  </term>
  
            <listitem>
    : sparse matrix (must be square)
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>alph  </term>
  
            <listitem>
    : diagonal compensation parameter (default: 0.5)
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>drop  </term>
  
            <listitem>
    : threshold parameter for dropping small terms in the factorization (default: 0.001*max(Aij))
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>L  </term>
  
            <listitem>
    : lower triangular sparse matrix
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>U  </term>
  
            <listitem>
    : upper triangular sparse matrix
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>ierr  </term>
  
            <listitem>
    : error message
  </listitem> 
  
         </varlistentry>
 
      </variablelist>
  
   </refsection>
  
   <refsection><title>Description</title>
  
      <para>
    Builds an incomplete LU factorization of the sparse matrix <literal>A</literal>. The two factors <literal>L</literal> and <literal>U</literal> are stored in CSR format. The CSR format is the Compressed Sparse Row format used by Scilab.
  </para>
  
      <para>
    All diagonal elements of the input matrix must be nonzero.
  </para>
 

  
      <listitem> 
    : The term <literal>alph</literal>*(sum of all dropped out elements in a given row) is added to the diagonal element of<literal>U</literal> of the factorization. Thus:
  </listitem>
  
      <para>
       0  --&gt; ILU with threshold,
  </para>
  
      <para>
       1  --&gt; MILU with threshold.
  </para>

  
      <listitem> 
    : During the elimination, a term a(i,j) is dropped whenever abs(a(i,j)) &lt; <literal>drop</literal> * [weighted norm of row i]. Here weighted norm = 1-norm / number of nnz elements in the row.
  </listitem>

  
      
  
      <para>
       0  --&gt; successful return.
  </para>
  
      <para>
      &gt;0  --&gt; zero pivot encountered at step number ierr.
  </para>
  
      <para>
      -1  --&gt; error. input matrix may be wrong. (The elimination process has generated a row in <literal>L</literal> or <literal>U</literal> whose length is .gt.  n.)
  </para>
  
      <para>
      -2  --&gt; insufficient storage for the LU factors caused an overflow in arrays alu/jalu.
  </para>
  
      <para>
      -3  --&gt; Zero row encountered.
  </para>
 
  
   </refsection>
  
   <refsection><title>Examples</title><programlisting role="example"><![CDATA[
A=mmread(SCILIN+'/tests/matrices/nos3.mtx')
n=size(A,1); b=ones(n,1);
alph=0;drop=0;
[L,U]=ilud(A,alph,drop);
x=U\(L\b)
A*x-b
 ]]></programlisting></refsection>
  
   <refsection><title>Authors</title><para>Sparskit procedure interfaced by Aladin Group  </para></refsection>
  
   <refsection><title>See Also</title><simplelist type="inline">
    
      <member> 
         <link linkend="ilu0">ilu0</link> 
      </member>     
      <member> 
         <link linkend="milu0">milu0</link> 
      </member>     
      <member> 
         <link linkend="iluk">iluk</link> 
      </member>     
      <member> 
         <link linkend="ilut">ilut</link> 
      </member>     
      <member> 
         <link linkend="ilutp">ilutp</link> 
      </member>     
      <member> 
         <link linkend="iludp">iludp</link> 
      </member>
  
   </simplelist></refsection>

</refentry>