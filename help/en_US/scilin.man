.TH Scilin G "April 2001" "Aladin Group" "Scilin Help"
.so ../sci.an
.SH NAME
scilin - available functions in a Scilab toolbox: Scilin, for iterative sparse systems.
.SH DESCRIPTION
PRECOND module:
.LP
ilut - incomplete LU factorization with dual truncation strategy
.LP
ilutp - ilut with column  pivoting
.LP
ilud - ilu with single dropping + diagonal compensation (~milut)
.LP
iludp - ilud with column pivoting
.LP
iluk - level-k ilu
.LP
ilu0 - simple ilu(0) preconditioning
.LP
milu0 - milu(0) preconditioning
.LP
SOLVERS module:
.LP
bicg - BiConjugate Gradient method
.LP
bicgstab - BiConjugate Gradient Stabilized method
.LP
cheby - chebyshev method with preconditionning
.LP
pcg - Conjugate Gradient method
.LP
cgs - Conjugate Gradient Squared method
.LP
gmres - Generalized Minimal RESidual method
.LP
jacobi - Jacobi method
.LP
qmr - Quasi Minimal Residual method
.LP
sor - Successive Over-Relaxation method
.LP
MATIO module:
.LP
mmread - reads contents of Matrix Market file into sparse or full matrix
.LP
mmwrite - writes sparse or dense matrix to Matrix Market formatted file
.LP
mminfo - extract size and storage information from a Matrix Market file
.LP
MATGEN module:
.LP
lehmer - a symmetric positive definite N-by-N matrix
.LP
makefish - make a Poisson matrix
.LP
wathen - a random N-by-N finite element matrix
.LP
gen57pt - generates 5-point and 7-point matrices
.fi
.SH AUTHOR
Aladin Group - 25 April 2001
