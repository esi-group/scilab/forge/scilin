.TH ilud G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
ilud - sparse incomplete lu factorization with single dropping and diagonal compensation
.SH CALLING SEQUENCE
.nf
[L,U,ierr]=ilud(A,alph,drop)
.fi
.SH PARAMETERS
.TP
A
: sparse matrix (must be square)
.TP
alph
: diagonal compensation parameter (default: 0.5)
.TP
drop
: threshold parameter for dropping small terms in the factorization (default: 0.001*max(Aij))
.TP
L
: lower triangular sparse matrix
.TP
U
: upper triangular sparse matrix
.TP
ierr
: error message
.SH DESCRIPTION
Builds an incomplete LU factorization of the sparse matrix \fBA\fP. The two factors \fBL\fP and \fBU\fP are stored in CSR format. The CSR format is the Compressed Sparse Row format used by Scilab.
.LP
All diagonal elements of the input matrix must be nonzero.
.TP 
alph
: The term \fBalph\fP*(sum of all dropped out elements in a given row) is added to the diagonal element of\fBU\fP of the factorization. Thus:
.LP
   0  --> ILU with threshold,
.LP
   1  --> MILU with threshold.
.TP
drop
: During the elimination, a term a(i,j) is dropped whenever abs(a(i,j)) < \fBdrop\fP * [weighted norm of row i]. Here weighted norm = 1-norm / number of nnz elements in the row.
.TP
ierr
: error flag
.LP
   0  --> successful return.
.LP
  >0  --> zero pivot encountered at step number ierr.
.LP
  -1  --> error. input matrix may be wrong. (The elimination process has generated a row in \fBL\fP or \fBU\fP whose length is .gt.  n.)
.LP
  -2  --> insufficient storage for the LU factors caused an overflow in arrays alu/jalu.
.LP
  -3  --> Zero row encountered.
.SH EXAMPLE
.nf
A=mmread(SCILIN+'/tests/matrices/nos3.mtx')
n=size(A,1); b=ones(n,1);
alph=0;drop=0;
[L,U]=ilud(A,alph,drop);
x=U\\(L\\b)
A*x-b
.fi
.SH AUTHOR
Sparskit procedure interfaced by Aladin Group
.SH SEE ALSO
ilu0, milu0, iluk, ilut, ilutp, iludp

