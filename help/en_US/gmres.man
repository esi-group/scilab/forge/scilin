.TH gmres G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
gmres - generalized minimum residual method with restarts
.SH CALLING SEQUENCE
.nf
[x,err,iter,flag,res] = gmres(A,b,x0,M,rstr,maxi,tol)
.fi
.SH PARAMETERS
.TP
A
: n-by-n matrix or function returning \fBA*x\fP
.TP
b
: right hand side vector
.TP
x0
: initial guess vector (default: zeros(n,1))
.TP
M
: preconditioner: matrix or function returning \fBM*x\fP (In the first case, default: eye(n,n))
.TP
rstr
: number of iterations between restarts (default: 10)
.TP
maxi
: maximum number of iterations (default: n)
.TP
tol
: error tolerance (default: 1000*%eps)
.TP
x
: solution vector
.TP
err
: final residual norm
.TP
iter
: number of iterations performed
.TP
flag
: 0 = \fBgmres\fP converged to the desired tolerance within \fBmaxi\fP iterations
.LP
  1 = no convergence given \fBmaxi\fP
.TP
res
: residual vector
.SH DESCRIPTION
Solves the linear system \fBAx=b\fP using the Generalized Minimal residual (GMRES(m)) method with restarts.
.SH EXAMPLE
.nf
A=mmread(SCILIN+'/tests/matrices/pde225.mtx');
n=siz(A,1);b=ones(n,1);x0=zeros(n,1);
[x,err,iter,flag,res] = gmres(A,b,x0)
M=eye(n,n); max_it=n; tol=1000*%eps;rstr=20;
[x,err,iter,flag,res] = gmres(A,b,x0,M,rstr,max_it,tol)

deff("y=precond(x)","y=(M+eye(size(M,1),size(M,2)))*x");
deff("y=matvec(x)","y=(A+eye(size(A,1),size(A,1)))*x");

[x,err,iter,flag,res] = gmres(matvec,b,x0,precond,rstr,max_it,tol)

[x,err,iter,flag,res] = gmres(A,b,x0,precond)
[x,err,iter,flag,res] = gmres(matvec,b,x0,M)
.fi
.SH AUTHOR
Adaptation by Aladin Group of the corresponding code of netlib/mltemplatesdev (Univ. of Tennessee and Oak Ridge National Laboratory) - 20 March 2001.
.SH SEE ALSO
bicg, bicgstab, cgs, qmr
