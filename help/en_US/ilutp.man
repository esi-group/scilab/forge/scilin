.TH ilutp G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
ilutp - sparse incomplete lu factorization with truncation and pivoting
.SH CALLING SEQUENCE
.nf
[L,U,perm,ierr]=ilutp(A,lfil,drop,ptol,bloc)
.fi
.SH PARAMETERS
.TP
A
: sparse matrix (must be square)
.TP
lfil
: the fill-in parameter(default: average number of nonzero entries per rows)
.TP
drop
: the threshold for dropping small terms in the factorization (default: 0.001*max(Aij))
.TP
ptol
: tolerance ratio used to determine whether or not to permute two columns (default: 0.5)
.TP
bloc
: permuting can be done within the diagonal blocks of size mbloc (default: n, with n the size of \fBA\fP)
.TP
L
: lower triangular sparse matrix
.TP
U
: upper triangular sparse matrix
.TP
perm
: contains the permutation arrays
.TP
ierr
: error message
.SH DESCRIPTION
Builds an incomplete LU factorization of a sparse matrix \fBA\fP. The two factors \fBL\fP and \fBU\fP are stored in CSR format. The CSR format is the Compressed Sparse Row format used by Scilab.
.TP 
lfil
: each row of \fBL\fP and each row of \fBU\fP will have a maximum of \fBlfil\fP elements (excluding the diagonal element). \fBlfil\fP must be .ge. 0.
.TP
ptol
: tolerance ratio used to  determine whether or not to permute two columns. At step i, columns i and j are permuted when abs(a(i,j))*\fBptol\fP > abs(a(i,i)). [0 --> never permute; recommended values from 0.1 to 0.01]
.TP
bloc
: useful for PDE problems with several degrees of freedom. To discard the feature, set \fBbloc\fP=n.
.TP
ierr
: error flag.
.LP
   0    --> successful return.
.LP
  >0  --> zero pivot encountered at step number ierr.
.LP
  -1   --> error. input matrix may be wrong. (The elimination process has generated a row in \fBL\fP or \fBU\fP whose length is .gt.  n.)
.LP
  -2   --> storage of matrix \fBL\fP caused an overflow in array al.
.LP
  -3   --> storage of matrix \fBU\fP caused an overflow in array alu.
.LP
  -4   --> illegal value for lfil.
.LP
  -5   --> zero row encountered.
.SH EXAMPLE
.nf
A=mmread(SCILIN+'/tests/matrices/pde225.mtx')
n=size(A,1); b=ones(n,1);
lfil=10;drop=0;ptol=0;bloc=3;
[L,U,perm,ierr]=ilutp(A,lfil,drop,ptol,bloc);
x=U\\(L\\b)
A*x-b
.fi
.SH AUTHOR
Sparskit procedure interfaced by Aladin Group
.SH SEE ALSO
ilu0, milu0, iluk, ilut, ilud, iludp

