.TH wathen G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
wathen -  a random N-by-N finite element matrix
.SH CALLING SEQUENCE
.nf
[A] = wathen(nx, ny, k)
.fi
.SH PARAMETERS
.TP
nx
: number of points in x
.TP
ny
: number of points in y
.TP
k
: 0 = normal
.LP
   1 = (diag(diag(A)) \ A) is returned
.TP
A
: the "consistent mass matrix" for a regular NX-by-NY grid of 8-node elements in 2 space dimensions.
.SH DESCRIPTION
Makes a random N-by-N finite element matrix where N = 3*NX*NY + 2*NX + 2*NY + 1. A is precisely the "consistent mass matrix" for a regular NX-by-NY grid of 8-node (serendipity) elements in 2 space dimensions. A is symmetric positive definite for any (positive) values of the "density", RHO(NX,NY), which is chosen randomly in this routine. In particular, if D=DIAG(DIAG(A)), then 0.25 <= EIG(INV(D)*A) <= 4.5 for any positive integers NX and NY and any densities RHO(NX,NY). This diagonally scaled matrix is returned by WATHEN(NX,NY,1).
.LP
BEWARE - this is a sparse matrix and it quickly gets large!
.SH EXAMPLE
.nf
nx=10;ny=10;k=1;
[A] = wathen(nx, ny, k)
.fi
.SH SEE ALSO
makefish, lehmer, matgen 