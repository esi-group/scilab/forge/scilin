<?xml version="1.0" encoding="ISO-8859-1"?><refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="gmres"><info><pubdate>March 2000  </pubdate></info><refnamediv><refname>gmres</refname><refpurpose> generalized minimum residual method with restarts  </refpurpose></refnamediv>
  
   
  
   
  
   
  
   
  
   
  
   <refsynopsisdiv><title>Calling Sequence</title><synopsis>[x,err,iter,flag,res] = gmres(A,b,x0,M,rstr,maxi,tol)</synopsis></refsynopsisdiv>
  
   <refsection><title>Parameters</title>
 
      <variablelist>
  
         <varlistentry>
  
            <term>A  </term>
  
            <listitem>
    : n-by-n matrix or function returning <literal>A*x</literal>
  
            </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>b  </term>
  
            <listitem>
    : right hand side vector
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>x0  </term>
  
            <listitem>
    : initial guess vector (default: zeros(n,1))
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>M  </term>
  
            <listitem>
    : preconditioner: matrix or function returning <literal>M*x</literal> (In the first case, default: eye(n,n))
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>rstr  </term>
  
            <listitem>
    : number of iterations between restarts (default: 10)
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>maxi  </term>
  
            <listitem>
    : maximum number of iterations (default: n)
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>tol  </term>
  
            <listitem>
    : error tolerance (default: 1000*%eps)
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>x  </term>
  
            <listitem>
    : solution vector
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>err  </term>
  
            <listitem>
    : final residual norm
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>iter  </term>
  
            <listitem>
    : number of iterations performed
  </listitem> 
  
         </varlistentry>
  
         <varlistentry>
  
            <term>flag  </term>
  
            <listitem>
    : 0 = <literal>gmres</literal> converged to the desired tolerance within <literal>maxi</literal> iterations
  </listitem> 
  
         </varlistentry>
  
         <para>
      1 = no convergence given <literal>maxi</literal>
  
         </para>
  
         <varlistentry>
  
            <term>res  </term>
  
            <listitem>
    : residual vector
  </listitem> 
  
         </varlistentry>
 
      </variablelist>
  
   </refsection>
  
   <refsection><title>Description</title>
  
      <para>
    Solves the linear system <literal>Ax=b</literal> using the Generalized Minimal residual (GMRES(m)) method with restarts.
  </para>
  
   </refsection>
  
   <refsection><title>Examples</title><programlisting role="example"><![CDATA[
A=mmread(SCILIN+'/tests/matrices/pde225.mtx');
n=siz(A,1);b=ones(n,1);x0=zeros(n,1);
[x,err,iter,flag,res] = gmres(A,b,x0)
M=eye(n,n); max_it=n; tol=1000*%eps;rstr=20;
[x,err,iter,flag,res] = gmres(A,b,x0,M,rstr,max_it,tol)

deff("y=precond(x)","y=(M+eye(size(M,1),size(M,2)))*x");
deff("y=matvec(x)","y=(A+eye(size(A,1),size(A,1)))*x");

[x,err,iter,flag,res] = gmres(matvec,b,x0,precond,rstr,max_it,tol)

[x,err,iter,flag,res] = gmres(A,b,x0,precond)
[x,err,iter,flag,res] = gmres(matvec,b,x0,M)
 ]]></programlisting></refsection>
  
   <refsection><title>Authors</title><para>Adaptation by Aladin Group of the corresponding code of netlib/mltemplatesdev (Univ. of Tennessee and Oak Ridge National Laboratory) - 20 March 2001.  </para></refsection>
  
   <refsection><title>See Also</title><simplelist type="inline">
    
      <member> 
         <link linkend="bicg">bicg</link> 
      </member>     
      <member> 
         <link linkend="bicgstab">bicgstab</link> 
      </member>     
      <member> 
         <link linkend="cgs">cgs</link> 
      </member>     
      <member> 
         <link linkend="qmr">qmr</link> 
      </member>
  
   </simplelist></refsection>

</refentry>