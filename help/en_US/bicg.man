.TH bicg G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
bicg - biconjugate gradient method with preconditioning.
.SH CALLING SEQUENCE
.nf
[x,err,iter,flag,res] = bicg(A,Ap,b,x0,M,Mp,maxi,tol)
.fi
.SH PARAMETERS
.TP
A
: matrix of size n-by-n or function returning \fBA*x\fP
.TP
Ap
: exists if \fBA\fP is a function. In this case \fBAp\fP is the transposed function of \fBA\fP, i.e returns \fBA'*x\fP
.TP
b
: right hand side vector
.TP
x0
: initial guess vector (default: zeros(n,1))
.TP
M
: preconditioner: matrix or function returning \fBM*x\fP (In the first case, default: eye(n,n))
.TP
Mp
: must only be provided when \fBM\fP is a function. In this case \fBMp\fP is the function which returns \fBM'*x\fP
.TP
maxi
: maximum number of iterations (default: n)
.TP
tol
: error tolerance (default: 1000*%eps)
.TP
x
: solution vector
.TP
err
: final residual norm
.TP
iter
: number of iterations performed
.TP
flag
: 0 = \fBbicg\fP converged to the desired tolerance within \fBmaxi\fP iterations
.LP
   1 = no convergence given \fBmaxi\fP
.LP
  -1 = breakdown
.TP
res
: residual vector
.SH DESCRIPTION
Solves the linear system \fBAx=b\fP using the BiConjugate Gradient Method with preconditioning.
.LP
.SH EXAMPLE
.nf
A=makefish(4); b=rand(16,1);x0=zeros(16,1);
[x,err,iter,flag,res] = bicg(A,b,x0)
M=eye(16,16); max_it=16; tol=1000*%eps;
[x,err,iter,flag,res] = bicg(A,b,x0,M,max_it,tol)

deff("y=precond(x)","y=(M+eye(size(M,1),size(M,2)))*x");
deff("y=precondp(x)","y=(M+eye(size(M,1),size(M,2)))''*x");

deff("y=matvec(x)","y=(A+eye(size(A,1),size(A,1)))*x");
deff("y=matvecp(x)","y=(A+eye(size(A,1),size(A,1)))''*x");

[x,err,iter,flag,res] = bicg(matvec,matvecp,b,x0,precond,precondp,max_it,tol)

[x,err,iter,flag,res] = bicg(matvec,matvecp,b,x0,M,max_it,tol)
[x,err,iter,flag,res] = bicg(A,b,x0,precond,precondp,max_it,tol)

.fi
.SH AUTHOR
Adaptation by Aladin Group of the corresponding code of netlib/mltemplatesdev (Univ. of Tennessee and Oak Ridge National Laboratory) - 20 March 2001.
.SH SEE ALSO
bicgstab, cgs, gmres, qmr