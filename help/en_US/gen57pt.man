.TH gen57pt G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
gen57pt - generates 5-point and 7-point matrices
.SH CALLING SEQUENCE
.nf
[Mat,rhs]=gen57pt(nx,ny,nz,al,mod)
.fi
.SH PARAMETERS
.TP
nx
: number of grid points in x direction
.TP
ny
: number of grid points in y direction
.TP
nz
: number of grid points in z direction
.TP
al
: array of size 6, carries the coefficient alpha of the boundary conditions
.TP
mod
: what to generate,
.LP
<0 : generate the graph only,
.LP
 0 : generate the matrix,
.LP
>0 : generate the matrix and the right-hand side.
.TP
Mat
: resulting sparse matrix 
.TP
rhs
: the right-hand side
.SH DESCRIPTION
Gives 5-pt and 7-pt matrices on rectangular regions
discretizing elliptic operators of the form: 
.LP
L u == delx( \fBA\fP delx u ) + dely ( \fBB\fP dely u) + delz ( \fBC\fP delz u ) + 
                delx ( \fBD\fP u ) + dely ( \fBE\fP u) + delz( \fBF\fP u ) + \fBG\fP u = \fBH\fP u
.LP
with Boundary conditions:
.LP
\fBalpha\fP del u / del n + \fBbeta\fP u = \fBgamma\fP
.LP
on a rectangular 1-D, 2-D or 3-D grid using centered difference scheme or upwind scheme.
.LP

The functions \fBA\fP, \fBB\fP, ..., \fBG\fP, \fBH\fP are known through the external subroutines \fBafun, bfun, ..., gfun, hfun\fP in the file 'functns.f' (SCILINDIR/routines/scilin/). The \fBalpha\fP is a constant on each side of the rectanglar domain. The \fBbeta\fP and the \fBgamma\fP are defined
by the external functions \fBbetfun\fP and \fBgamfun\fP (see 'functns.f' for examples).
.SH EXAMPLE
nx=10;ny=10;nz=10;
al=zeros(1,6);
mod=1;
[A,rhs]=gen57pt(nx,ny,nz,al,mod)
.nf
.fi
.SH AUTHOR
Sparskit procedure interfaced by Aladin Group
