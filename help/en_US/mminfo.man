.TH mminfo G "May 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
mminfo - reads the contents of the Matrix Market file 'filename' and extracts informations on size and storage
.SH CALLING SEQUENCE
.nf
[rows, cols, entr, rep, typ, symm] = mminfo(file)
.fi
.SH PARAMETERS
.TP
file
: the name of the Matrix Market file
.TP
rows
: number of rows of the matrix
.TP
cols
: number of columns of the matrix
.TP
entr
: number of nonzero entries stored in the file
.TP
rep
: representation of the Matrix Market format
.LP
  -'coordinate', coordinate sparse storage
.LP
  -'array', dense array storage
.TP
typ
: type of the entries: 'real', 'complex', 'integer', pattern'
.TP
symm
: gives some informations about the symmetry of the matrix: 'general', 'symmetric', 'hermitian', 'skew-symmetric'
.SH DESCRIPTION
Reads the contents of the Matrix Market file 'file' and extracts informations on size and storage. For coordinate sparse storage, \fBentr\fP refers to the number of nonzero entries stored in the file. The \fBnnz\fP command
determines the final number of nonzero entries in the matrix after the data extraction. For array sparse storage, \fBentr\fP is the product \fBrows*cols\fP and represents the final number of nonzero entries in the matrix too.
.LP
.SH EXAMPLE
.nf
[rows,cols] = mminfo('test.mtx')
[rows,cols,entr,rep,typ,symm] = mminfo('test.mtx')
The file 'test.mtx' has been priorly downloaded from the Matrix Market home page. 
.fi
.SH AUTHOR
Adaptation by Aladin of the corresponding code of Matrix Market set - 14 May 2001.
.SH SEE ALSO
mmread, mmwrite

