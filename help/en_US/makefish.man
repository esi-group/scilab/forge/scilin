.TH makefish G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
makefish - make a 2D Poisson matrix
.SH CALLING SEQUENCE
.nf
[A] = makefish(siz)
.fi
.SH PARAMETERS
.TP
siz
: size of \fBA\fP
.TP
A
: a 2D Poisson matrix
.SH DESCRIPTION
Make a Poisson matrix
.SH EXAMPLE
siz=10;
[A] = makefish(siz)
.fi
.SH AUTHOR
Adaptation by Aladin Group of the corresponding code of netlib/mltemplatesdev (Univ. of Tennessee and Oak Ridge National Laboratory) - 20 March 2001.
.SH SEE ALSO
lehmer, wathen, matgen 