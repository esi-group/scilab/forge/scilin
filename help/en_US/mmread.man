.TH mmread G "May 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
mmread - reads the contents of a Matrix Market file into a matrix (sparse or dense format)
.SH CALLING SEQUENCE
.nf
[A] = mmread(file)
.fi
.SH PARAMETERS
.TP
file
: name of the Matrix Market file
.TP
A
: sparse or full matrix
.SH DESCRIPTION
Reads the contents of the Matrix Market file 'filename' into the matrix \fBA\fP. The storage of \fBA\fP will be either sparse or dense, depending on the Matrix Market format. Storage and size information about the matrix can be obtained by using the 'mminfo' function.
.LP
.SH EXAMPLE
.nf
A = mmread('test.mtx')
The file 'test.mtx' has been priorly downloaded from the Matrix Market home page. 

.fi
.SH AUTHOR
Adaptation by Aladin of the corresponding code of Matrix Market set - 14 May 2001.
.SH SEE ALSO
mmwrite, mminfo