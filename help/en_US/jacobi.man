.TH jacobi G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
jacobi - jacobi method
.SH CALLING SEQUENCE
.nf
[x,err,iter,flag,res]  = jacobi(A,b,x0,maxi,tol)
.fi
.SH PARAMETERS
.TP
A
: matrix of size n-by-n or function returning \fBA*x\fP
.TP
b
: right hand side vector
.TP
x0
: initial guess vector (default: zeros(n,1))
.TP
maxi
: maximum number of iterations (default: n)
.TP
tol
: error tolerance (default: 1000*%eps)
.TP
x
: solution vector
.TP
err
: final residual norm
.TP
iter
: number of iterations performed
.TP
flag
: 0 = \fBjacobi\fP converged to the desired tolerance within \fBmaxi\fP iterations
.LP
  1 = no convergence given \fBmaxi\fP
.TP
res
: residual vector
.SH DESCRIPTION
Solves the linear system \fBAx=b\fP using the Jacobi Method.
.SH EXAMPLE
.nf
A=makefish(4); b=rand(16,1);x0=zeros(16,1);
[x,err,iter,flag,res] = jacobi(A,b,x0)
max_it=16; tol=1000*%eps;
[x,err,iter,flag,res] = jacobi(A,b,x0,max_it,tol)

deff("y=matvec(x)","y=(A+eye(size(A,1),size(A,1)))*x");

[x,err,iter,flag,res] = jacobi(matvec,b,x0,max_it,tol)
[x,err,iter,flag,res] = jacobi(matvec,b,x0)
.fi
.SH AUTHOR
Adaptation by Aladin Group of the corresponding code of netlib/mltemplatesdev (Univ. of Tennessee and Oak Ridge National Laboratory) - 20 March 2001.
.SH SEE ALSO
sor