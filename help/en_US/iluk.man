.TH iluk G "March 2000" "Scilab Group" "Scilab Function"
.so ../sci.an
.SH NAME
iluk - level-k of the sparse incomplete lu factorization with dual truncation
.SH CALLING SEQUENCE
.nf
[L,U,ierr]=iluk(A,lfil,elts)
.fi
.SH PARAMETERS
.TP
A
: sparse matrix (must be squared)
.TP
lfil
: the fill-in parameter; The default of \fBlfil\fP is the average number of nonnull elements of the matrix A by lines
.TP
elts
: maximal number of nonnull elements of the matrices \fBL\fP and \fBU\fP, without diagonal elements; The default of \fBelts\fP is always correct, but over-estimates a lot.
.TP
L
: lower triangular sparse matrix
.TP
U
: upper triangular sparse matrix
.TP
ierr
: error flag
.SH DESCRIPTION
Builds an incomplete LU factorization of the sparse matrix \fBA\fP. The two factors \fBL\fP and \fBU\fP are stored under a CSR format. The CSR format is the Compressed Sparse Row format used by Scilab.
.LP
All the diagonal elements of the input matrix must be nonzero.
.TP 
lfil
: each row of \fBL\fP and each row of \fBU\fP will have a maximum of \fBlfil\fP elements (excluding the diagonal element). \fBlfil\fP must be .ge. 0.
.TP
ierr
: error flag
.LP
   0    --> successful return.
.LP
  >0  --> zero pivot encountered at step number ierr.
.LP
  -1   --> error. input matrix may be wrong. (The elimination process has generated a row in \fBL\fP or \fBU\fP whose length is .gt.  n.)
.LP
  -2   --> storage of matrix \fBL\fP caused an overflow in array al.
.LP
  -3   --> storage of matrix \fBU\fP caused an overflow in array alu.
.LP
  -4   --> illegal value for \fBlfil\fP.
.LP
  -5   --> zero row encountered in \fBA\fP or \fBU\fP.
.SH EXAMPLE
.nf
A=mmread(SCILIN+'/tests/matrices/pde225.mtx')
n=size(A,1); b=ones(n,1);
lfil=10;
[L,U,ierr]=iluk(A,lfil);
x=U\\(L\\b)
A*x-b
.fi
.SH AUTHOR
Sparskit procedure interfaced by Aladin Group
.SH SEE ALSO
ilu0, milu0, ilut, ilud, ilutp, iludp
