function y=%sp_length(A)
[ij,v,mn]=spget(A);
y=length(v);
