function [x, err, iter, flag, res]  = jacobi(Mat, varargin)

//  -- Iterative template routine --
//     Univ. of Tennessee and Oak Ridge National Laboratory
//     October 1, 1993
//     Details of this algorithm are described in "Templates for the
//     Solution of Linear Systems: Building Blocks for Iterative
//     Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra,
//     Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications,
//     1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
//
// [x, err, iter, flag, res]  = jacobi(A, b, x, max_it, tol)
//
// jacobi.m solves the linear system Ax=b using the Jacobi Method.
//
// input   Mat      REAL matrix
//         x        REAL initial guess vector
//         b        REAL right hand side vector
//         max_it   INTEGER maximum number of iterations
//         tol      REAL error tolerance
//
// output  x        REAL solution vector
//         err      REAL final residual norm
//         iter     INTEGER number of iterations performed
//         flag     INTEGER: 0 = solution found to tolerance
//                           1 = no convergence given max_it
//         res      REAL residual vector



//========================================================
//========================================================
//
//               Parsing input arguments.
//
//========================================================
//========================================================

[lhs,rhs]=argn(0);
if ( rhs== 0 ),
  error("jacobi: matrix is expected",502);
end

if (rhs == 1),
   error("Please enter right hand side vector b");
end 

//--------------------------------------------------------
// Parsing of the matrix A
//--------------------------------------------------------

if ( type(Mat) ~=1 & type(Mat) ~= 5 ),
  error("jacobi: matrix is expected",502);
end
if (size(Mat,1) ~= size(Mat,2)),
  error("jacobi: matrix A must be square",502);
end

//--------------------------------------------------------
// Parsing of the right hand side b
//--------------------------------------------------------

b=varargin(1);
if ( size(b,2) ~= 1 ),
   error("jacobi: right hand side member must be a column vector",502);
end
if ( size(b,1) ~= size(Mat,1) ),
   error("jacobi: right hand side member must have the size of the matrix A",502);
end 

//--------------------------------------------------------
// Parsing of the initial vector x
//--------------------------------------------------------

if (rhs >= 3),
  x=varargin(2);
  if (size(x,2) ~= 1),
    error("jacobi: initial guess x0 must be a column vector",502);
  end
  if ( size(x,1) ~= size(b,1) ),
    error("jacobi: initial guess x0 must have the size of b",502);
  end 
else
  x=zeros(size(b,1),1);
end

//--------------------------------------------------------
// Parsing of the maximum number of iterations max_it
//--------------------------------------------------------

if (rhs >= 4),
   max_it=varargin(3);
   if (size(max_it,1) ~= 1 | size(max_it,2) ~=1),
      error("jacobi: max_it must be a scalar",502);
   end 
else
   max_it=size(b,1);
end

//--------------------------------------------------------
// Parsing of the error tolerance tol
//--------------------------------------------------------

if (rhs == 5),
   tol=varargin(4);
   if (size(tol,1) ~= 1 | size(tol,2) ~=1),
      error("jacobi: tol must be a scalar",502);
   end
else
   tol=1000*%eps;
end

//--------------------------------------------------------
// test about input arguments number
//--------------------------------------------------------

if (rhs > 5),
   error("jacobi: too many input arguments",502);
end
 
//========================================================
//========================================================
//
//                Begin of computations
//
//========================================================
//======================================================== 
 
  i = 0;                                       // initialization
  flag = 0;

  bnrm2 = norm( b );
  if  ( bnrm2 == 0.0 ), bnrm2 = 1.0; end

  r = b - A*x;
  err = norm( r ) / bnrm2;
  res = err;
  if ( err < tol ),return; end

  [m,n]=size(A);
  [ M, N ] = split( A , b, 1.0, 1 );              // matrix splitting

  for i = 1:max_it,                            // begin iteration

     x_1 = x;
     x   = M \ (N*x + b);                         // update approximation

     err = norm( x - x_1 ) / norm( x );         // compute error
     res = [res;err];
     if ( err <= tol ), iter=i; break; end              // check convergence

     if ( i == max_it ), iter=i; end
  end

  if ( err > tol ),flag = 1; end                // no convergence

// END jacobi.sci

