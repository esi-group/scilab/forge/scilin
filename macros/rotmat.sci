function [ c, s ] = rotmat( a, b )

//
// Compute the Givens rotation matrix parameters for a and b.
//

[lhs,rhs]=argn(0);

if( rhs== 0 ),
   error("Scalar is expected");
end

if( rhs >= 1 ),

   if ( size(a,1) ~= 1 | size(a,2) ~= 1 ),
      error("a must be scalar");
   end

end

if( rhs == 2 ),

   if ( size(b,1) ~= 1 | size(b,2) ~= 1 ),
      error("b must be a scalar");
   end

end

if ( rhs > 2 ),
   error("Too input arguments");
end

// begin of computations

   if ( b == 0.0 ),
      c = 1.0;
      s = 0.0;
   elseif ( abs(b) > abs(a) ),
      temp = a / b;
      s = 1.0 / sqrt( 1.0 + temp^2 );
      c = temp * s;
   else
      temp = b / a;
      c = 1.0 / sqrt( 1.0 + temp^2 );
      s = temp * c;
   end

//END rotmat.sci
   
