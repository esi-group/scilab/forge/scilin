function A=nonsym(a,b,n);
h=1/(n+1);
y=(2+a*h+b*h^2) ; x=-(1+a*h); z=-1;

x = x*ones(n-1,1);
z = z*ones(n-1,1);
y = y*ones(n,1);

A = diag(x, -1) + diag(y) + diag(z, 1);
 

