#include "stack-c.h"
#include "conv.h"
#include "ilut.h"

extern int intiluk(char *fname)
{ 
  int mA,nA,*ia,mlf=1,nlf=1,plfil,mmaxf=1,nmaxf=1,pmaxf,pierr,mierr=1,nierr=1;
  int iwk, *jw, *jlu, *ju, *levs;
  double *w,*alu;
  SciSparse A,*L,*U;
  
  CheckRhs(1,3);
  CheckLhs(2,3);
  if (VarType(1)==5){
    GetRhsVar(1,"s",&mA,&nA,&A);
    if (mA!=nA){
      Scierror(501,"%s: input matrix must be square \r\n",fname);
    }
  } else {
    Scierror(501,"%s: input matrix must be sparse \r\n",fname);
    return 0;
  }
  if (Rhs>=2) {
    if (VarType(2)==1){
      GetRhsVar(2,"i",&mlf,&nlf,&plfil);
      if ((mlf!=1)||(nlf!=1)){
	Scierror(501,"%s: lfil must be an integer \r\n",fname);
      }
    } else {
      Scierror(501,"%s: lfil must be an integer \r\n",fname);
      return 0;
    }
  } else {
    CreateVar(2,"i",&mlf,&nlf,&plfil);
    *istk(plfil)=(int) 1;
  }
  if (Rhs>=3) {
    if (VarType(3)==1){
      GetRhsVar(3,"i",&mmaxf,&nmaxf,&pmaxf);
      if ((mmaxf!=1)||(nmaxf!=1)){
	Scierror(501,"%s: maxfil must be an integer \r\n",fname);
      }
    } else {
      Scierror(501,"%s: maxfil must be an integer \r\n",fname);
      return 0;
    }
  } else {
    CreateVar(3,"i",&mmaxf,&nmaxf,&pmaxf);
    *istk(pmaxf)= lband(A);
  }
  CreateVar(4,"i",&mierr,&nierr,&pierr);
  
  iwk=*istk(pmaxf)+A.m+1;
  jw=(int *) malloc(3*A.m*sizeof(int));
  w= (double *) malloc(A.m*sizeof(double));
  ia=Sci2spk(&A);    
  alu= (double *) malloc(iwk*sizeof(double));
  jlu= (int *) malloc(iwk*sizeof(int));
  levs= (int *) malloc(iwk*sizeof(int));
  ju= (int *) malloc((A.m)*sizeof(int));
  
  iluk_(&A.m,A.R,A.icol,ia,istk(plfil),alu,jlu,ju,levs,&iwk,w,jw,istk(pierr));

  free(w);
  free(jw);
  free(levs);
  free(ia);

  if (*istk(pierr)!=0){
    free(ju);
    free(jlu);
    free(alu);
    if (*istk(pierr)==-1) {
      Scierror(501,"%s: input matrix may be wrong \r\n",fname);
    } else if (*istk(pierr)==-2) {
      Scierror(501,"%s: not enough memory for matrix L \r\n",fname);
    } else if (*istk(pierr)==-3) {
      Scierror(501,"%s: not enough memory for matrix U \r\n",fname);
    } else if (*istk(pierr)==-4) {
      Scierror(501,"%s: illegal value for lfil \r\n",fname);
    } else if (*istk(pierr)==-5) {
      Scierror(501,"%s: zero row encountered in A or U \r\n",fname);
    } else {
      Scierror(501,"%s: zero pivot encountered at step number %d \r\n",fname,*istk(pierr));
    }
  }
  else {
    spluget(A.m,ju,jlu,alu,&L,&U);
    free(ju);
    free(jlu);
    free(alu);
    CreateVarFromPtr(5,"s",&L->m,&L->n,L);
    CreateVarFromPtr(6,"s",&U->m,&U->n,U);
    LhsVar(1) =5;
    LhsVar(2) =6;
    LhsVar(3) =4;
  }
  return(0);
}
