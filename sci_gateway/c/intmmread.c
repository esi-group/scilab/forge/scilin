#include <stdio.h>
#include <stack-c.h>
#include <malloc.h>
#include "mmio.h"
#include "sort.h"

extern int intmmread(char *fname)
{
  MM_typecode matcode;
  FILE *f;
  int i, *temp;
  SciSparse *A;
  int ret_code, un = 1;
  int m1, n1, l1, m2, n2, l2;
  int nz, k=0;
  
  CheckRhs(1,1);
  CheckLhs(1,4);
  
  if (VarType(1)==10){
    GetRhsVar(1,"c",&m1,&n1,&l1);
  }
  
  if ( (f=fopen(cstk(l1),"r")) == NULL ){
    Scierror(503,"Problem opening file");
    return(0);
  }

  if (mm_read_banner(f, &matcode) != 0){
    Scierror(503,"Could not process Matrix Market banner.\n");
    return(0);
  }

  if ( mm_is_coordinate(matcode) ){

    A=(SciSparse *) malloc (sizeof(SciSparse));
    A->it=0;
    if ((ret_code = mm_read_mtx_crd_size(f,&A->m,&A->n,&A->nel)) !=0){ 
      Scierror(503,"Problem reading size information");
      return(0);
    }
    A->mnel=(int *) malloc(A->m*sizeof(int));
    nz=A->nel;
    if (mm_is_symmetric(matcode) || mm_is_skew(matcode) || mm_is_hermitian(matcode))
      A->nel=2*A->nel;
    A->icol = (int *) malloc(A->nel*sizeof(int));
    A->R = (double *) malloc(A->nel*sizeof(double));
    temp = (int *) malloc(A->nel*sizeof(int));
    for (i=0; i<A->m; i++){
      A->mnel[i]=0;
    }
    if (mm_is_real(matcode) || mm_is_integer(matcode)){

      if(mm_is_general(matcode)){
	for (i=0; i<A->nel; i++){
	  if (fscanf(f, "%d %d %lg\n", temp+i, A->icol+i, A->R+i) !=3){
	    Scierror(503,"%s: unexpected EOF",fname);
            return(0);
	  }
	  A->mnel[temp[i]-1]++;
	}
	quick_sort_real(temp, A->icol, A->R,A->nel);
      } else if( mm_is_symmetric(matcode)){
        for (i=0; i<nz; i++,k++){
          if (fscanf(f, "%d %d %lg\n", temp+k, A->icol+k, A->R+k) !=3){
	    Scierror(503,"%s: unexpected EOF",fname);
            return(0);
	  }
          A->mnel[temp[k]-1]++;
	  if (temp[k] == A->icol[k]){
            (A->nel)--;
          } else {
            temp[k+1]=A->icol[k];
            A->mnel[temp[k+1]-1]++;
            A->icol[k+1]=temp[k];
            A->R[k+1]=A->R[k];
            k++;
          }
	}
	quick_sort_real(temp, A->icol, A->R,A->nel);
      } else if( mm_is_skew(matcode)){
	for (i=0; i<nz; i++,k++){
	  if (fscanf(f, "%d %d %lg\n", temp+k, A->icol+k, A->R+k) !=3){
	    Scierror(503,"%s: unexpected EOF",fname);
	    return(0);
	  }
	  A->mnel[temp[k]-1]++;
	  if (temp[k] == A->icol[k]){
	    (A->nel)--;
	  } else {
	    temp[k+1]=A->icol[k];
	    A->mnel[temp[k+1]-1]++;
	    A->icol[k+1]=temp[k];
	    A->R[k+1]=-(A->R[k]);
	    k++;
	  }
 	}
 	quick_sort_real(temp, A->icol, A->R,A->nel);
      } else if ( mm_is_hermitian(matcode)){
	Scierror(503,"Hermitian type is not allowed with Real or Integer field");
        return(0);
      }
    }
    else if (mm_is_complex(matcode)){
      
      A->it=1;
      A->I = (double *) malloc(A->nel*sizeof(double));
      if (mm_is_general(matcode)){ 
  	for (i=0; i<nz; i++){ 
  	  if (fscanf(f, "%d %d %lg %lg\n", temp+i, A->icol+i, A->R+i, A->I+i) != 4){
  	    Scierror(503,"%s: unexpected EOF",fname);
            return(0);
	  }
  	  A->mnel[temp[i]-1]++; 
  	}
	quick_sort_complex(temp, A->icol, A->R, A->I,A->nel);
      } else if (mm_is_symmetric(matcode)){ 
  	for (i=0; i<nz; i++,k++){ 
  	  if (fscanf(f, "%d %d %lg %lg\n", temp+k, A->icol+k, A->R+k, A->I+k) != 4){
  	    Scierror(503,"%s: unexpected EOF",fname);
	    return(0);
	  }
  	  A->mnel[temp[k]-1]++; 
  	  if (temp[k] == A->icol[k]){ 
  	    (A->nel)--;
	  } else { 
  	    temp[k+1]=A->icol[k];
	    A->mnel[temp[k+1]-1]++;
            A->icol[k+1]=temp[k];
            A->R[k+1]=A->R[k];
            A->I[k+1]=A->I[k];
	    k++; 
  	  } 
  	}
  	quick_sort_complex(temp, A->icol, A->R, A->I,A->nel);
      } else if (mm_is_skew(matcode)){ 
  	for (i=0; i<nz; i++,k++){ 
  	  if (fscanf(f, "%d %d %lg %lg\n", temp+k, A->icol+k, A->R+k, A->I+k) != 4){ 
  	    Scierror(503,"%s: unexpected EOF",fname);
            return(0);
	  }
  	  A->mnel[temp[k]-1]++; 
  	  if (temp[k] == A->icol[k]){ 
  	    (A->nel)--;
	  } else { 
  	    temp[k+1]=A->icol[k];
	    A->mnel[temp[k+1]-1]++;
            A->icol[k+1]=temp[k];
            A->R[k+1]=-A->R[k];
            A->I[k+1]=-A->I[k];
	    k++; 
  	  } 
  	}
  	quick_sort_complex(temp, A->icol, A->R, A->I,A->nel);
      } else if (mm_is_hermitian(matcode)){ 
  	for (i=0; i<nz; i++,k++){ 
  	  if (fscanf(f, "%d %d %lg %lg\n", temp+k, A->icol+k, A->R+k, A->I+k) != 4){ 
  	    Scierror(503,"%s: unexpected EOF",fname);
            return(0);
	  }
  	  A->mnel[temp[k]-1]++; 
  	  if (temp[k] == A->icol[k]){ 
  	    (A->nel)--;
	  } else { 
  	    temp[k+1]=A->icol[k];
	    A->mnel[temp[k+1]-1]++;
            A->icol[k+1]=temp[k];
            A->R[k+1]=A->R[k];
            A->I[k+1]=-A->I[k];
	    k++; 
  	  } 
  	}
  	quick_sort_complex(temp, A->icol, A->R, A->I,A->nel);
      }
    } else if (mm_is_pattern(matcode)){ 

      if( mm_is_general(matcode)){
  	for (i=0; i<nz; i++){
  	  if (fscanf(f, "%d %d", temp+i, A->icol+i) != 2){ 
  	    Scierror(503,"%s: unexpected EOF",fname);
            return(0);
	  }
  	  A->R[i]=1; 
  	  A->mnel[temp[i]-1]++;
  	}
  	quick_sort_patern(temp, A->icol, A->nel);
      } else if(mm_is_symmetric(matcode)){
  	for (i=0; i<nz; i++,k++){
  	  if (fscanf(f, "%d %d", temp+k, A->icol+k) != 2){
  	    Scierror(503,"%s: unexpected EOF",fname);
            return(0);
	  }
  	  A->R[k]=1; 
  	  A->mnel[temp[k]-1]++; 
            if (temp[k] == A->icol[k]){ 
  	    (A->nel)--; 
  	  } else { 
  	    temp[k+1]=A->icol[k];
	    A->mnel[temp[k+1]-1]++;
            A->icol[k+1]=temp[k]; 
  	    A->R[k+1]=1; 
  	    k++; 
  	  } 
  	} 
  	quick_sort_patern(temp, A->icol, A->nel); 
      } else if (mm_is_skew(matcode) || mm_is_hermitian(matcode)){
	Scierror(503,"Skew ou Hermitian type is not allowed with Pattern field");
        return(0);
      } 
    }
    
    CreateVarFromPtr(2,"s",&A->m,&A->n,A);
   
  } else if ( mm_is_array(matcode) ){ 
    
    if ((ret_code = mm_read_mtx_array_size(f,&m2,&n2)) !=0){ 
      Scierror(503,"Problem reading size information"); 
      return(0);
    }
    if (mm_is_real(matcode) || mm_is_integer(matcode)){
      
      if(mm_is_general(matcode)){
  	CreateVar(2,"d",&m2,&n2,&l2) 
  	  for (i=0; i<m2*n2; i++){ 
  	    fscanf(f,"%lg\n",stk(l2)+i);
  	  } 
      } else if (mm_is_symmetric(matcode)){
      	CreateVar(2,"d",&m2,&n2,&l2) 
  	  for (k=0; k<n2; k++){ 
  	    fscanf(f,"%lg\n",stk(l2)+k*m2+k);
	    for (i=k+1;i<m2;i++){
  	      fscanf(f,"%lg\n",stk(l2)+k*m2+i);
	      stk(l2)[i*m2+k] = stk(l2)[k*m2+i];
  	    } 
  	  }
      } else if (mm_is_skew(matcode)){
	CreateVar(2,"d",&m2,&n2,&l2) 
  	  for (k=0; k<n2; k++){ 
  	    fscanf(f,"%lg\n",stk(l2)+k*m2+k);
	    for (i=k+1;i<m2;i++){ 
  	      fscanf(f,"%lg\n",stk(l2)+k*m2+i); 
	      stk(l2)[i*m2+k] = -stk(l2)[k*m2+i]; 
  	    } 
  	  } 
      } 
    } else if (mm_is_complex(matcode)){ 
      
      if(mm_is_general(matcode)){ 
	CreateVar(2,"z",&m2,&n2,&l2)
	  for (k=0; k<n2 ; k++){ 
	    for (i=0;i<m2;i++){ 
	      fscanf(f,"%lg %lg\n",stk(l2)+2*k*m2+2*i,stk(l2)+2*k*m2+2*i+1);
	    }
	  } 
      } else 	if(mm_is_symmetric(matcode)){ 
	CreateVar(2,"z",&m2,&n2,&l2)
	  for (k=0; k<n2 ; k++){ 
	    fscanf(f,"%lg %lg\n",stk(l2)+2*k*m2+2*k,stk(l2)+2*k*m2+2*k+1);
	    for (i=k+1;i<m2;i++){ 
	      fscanf(f,"%lg %lg\n",stk(l2)+2*k*m2+2*i,stk(l2)+2*k*m2+2*i+1);
	      stk(l2)[2*i*m2+2*k]=stk(l2)[2*k*m2+2*i];
	      stk(l2)[2*i*m2+2*k+1]=stk(l2)[2*k*m2+2*i+1];
	    }
	  } 
      } else if(mm_is_skew(matcode)){ 
	CreateVar(2,"z",&m2,&n2,&l2)
	  for (k=0; k<n2 ; k++){ 
	    fscanf(f,"%lg %lg\n",stk(l2)+2*k*m2+2*k,stk(l2)+2*k*m2+2*k+1);
	    for (i=k+1;i<m2;i++){ 
	      fscanf(f,"%lg %lg\n",stk(l2)+2*k*m2+2*i,stk(l2)+2*k*m2+2*i+1);
	      stk(l2)[2*i*m2+2*k]=-stk(l2)[2*k*m2+2*i];
	      stk(l2)[2*i*m2+2*k+1]=-stk(l2)[2*k*m2+2*i+1];
	    }
	  }
      } else if(mm_is_hermitian(matcode)){ 
	CreateVar(2,"z",&m2,&n2,&l2)
	  for (k=0; k<n2 ; k++){ 
	    fscanf(f,"%lg %lg\n",stk(l2)+2*k*m2+2*k,stk(l2)+2*k*m2+2*k+1);
	    for (i=k+1;i<m2;i++){ 
	      fscanf(f,"%lg %lg\n",stk(l2)+2*k*m2+2*i,stk(l2)+2*k*m2+2*i+1);
	      stk(l2)[2*i*m2+2*k]=stk(l2)[2*k*m2+2*i];
	      stk(l2)[2*i*m2+2*k+1]=-stk(l2)[2*k*m2+2*i+1];
	    }
	  }
      } else if(mm_is_pattern(matcode)){
	Scierror(503,"The field Pattern is not allowed in the Array format");
	return(0);
      }
    }
  }
  
  fclose(f);
  LhsVar(1)=2;
  
  return 0;
}

