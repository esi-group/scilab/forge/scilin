#include <stdio.h>
#include <stack-c.h>
#include <malloc.h>
#include <string.h>
#include "mmio.h"

extern int intmminfo(char *fname)
{
  MM_typecode matcode;
  FILE *f;
  int ret_code;
  int m1, n1, l1, l2, l3, l4, l5, l6, l7;
  int un=1, quatre=4, cinq=5, sept=7, neuf=9, dix=10,quatorze=14;
 
  CheckRhs(1,1);
  CheckLhs(1,6);
  
  if (VarType(1)==10){
    GetRhsVar(1,"c",&m1,&n1,&l1);
  }
  
  f=fopen(cstk(l1),"r");

  if (f == NULL){
    Scierror(503,"%s: problem opening file \r\n",fname);
    return(0);
  }

  if (mm_read_banner(f, &matcode) != 0)
    {
      Scierror(503,"%s: could not process Matrix Market banner \r\n",fname);
      return(0);
     }

  if (!mm_is_matrix(matcode))  
    {
      Scierror(503,"%s: not a matrix \r\n",fname);
      return(0);
    }

  if (mm_is_sparse(matcode)){
    CreateVar(2,"c",&un,&dix,&l2);
    strcpy(cstk(l2),"coordinate");    
  } else if (mm_is_dense(matcode)){
    CreateVar(2,"c",&un,&cinq,&l2);
    strcpy(cstk(l2),"array");  
  } else {
    Scierror(503,"%s: not a valid format \r\n",fname);
    return(0);
  }

  if (mm_is_real(matcode)){
    CreateVar(3,"c",&un,&quatre,&l3);
    strcpy(cstk(l3),"real");
  } else if  (mm_is_integer(matcode)){
    CreateVar(3,"c",&un,&sept,&l3);
    strcpy(cstk(l3),"integer");
  } else if  (mm_is_complex(matcode)){
    CreateVar(3,"c",&un,&sept,&l3);
    strcpy(cstk(l3),"complex");
  } else if  (mm_is_pattern(matcode)){
    CreateVar(3,"c",&un,&sept,&l3);
    strcpy(cstk(l3),"pattern");
  } else {
    Scierror(503,"%s: not a valid type \r\n",fname);
    return(0);
  }

  if (mm_is_symmetric(matcode)){
    CreateVar(4,"c",&un,&neuf,&l4);
    strcpy(cstk(l4),"symmetric");
  } else if (mm_is_general(matcode)){
    CreateVar(4,"c",&un,&sept,&l4);
    strcpy(cstk(l4),"general");
  } else if (mm_is_hermitian(matcode)){
    CreateVar(4,"c",&un,&neuf,&l4);
    strcpy(cstk(l4),"hermitian");
  } else if (mm_is_skew(matcode)){
    CreateVar(4,"c",&un,&quatorze,&l4);
    strcpy(cstk(l4),"skew-symmetric");
  } else {
    Scierror(503,"%s: not a valid symmetrie \r\n",fname);
    return(0);
  }
      
  if (mm_is_dense(matcode)){
    CreateVar(5,"i",&un,&un,&l5);    
    CreateVar(6,"i",&un,&un,&l6);
    CreateVar(7,"i",&un,&un,&l7);
    if ((ret_code = mm_read_mtx_array_size(f,istk(l5),istk(l6))) !=0){
      Scierror(503,"%s: problem reading size information \r\n",fname);
      return(0);
    }
    *istk(l7)=*istk(l5) * *istk(l6);
  } else {
    CreateVar(5,"i",&un,&un,&l5);    
    CreateVar(6,"i",&un,&un,&l6);
    CreateVar(7,"i",&un,&un,&l7);
    if ((ret_code = mm_read_mtx_crd_size(f,istk(l5),istk(l6),istk(l7))) !=0){ 
      Scierror(503,"%s: problem reading size information \r\n",fname);
      return(0);
    }
  }
   
  fclose(f);

  LhsVar(3)=7;
  LhsVar(2)=6;
  LhsVar(1)=5;
  LhsVar(6)=4;
  LhsVar(5)=3;
  LhsVar(4)=2;

  return 0;

}
