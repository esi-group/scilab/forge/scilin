#include <stdio.h>
#include <stack-c.h>
#include <malloc.h>
#include "mmio.h"

extern int intmmwrite(char *fname)
{
  MM_typecode matcode;
  FILE *f;
  int row,i,index=0, err=0;
  int m1, n1, l1;
  int m2, n2, l2, l3;
  int un=1;
  SciSparse A;
  int it1, l, *l0;
   
  /* adresses of the bounds of the locations of the right hand side parameter */

  CheckRhs(2,2);
  CheckLhs(1,1);

  Rhs=2;
  l0=lstk(Top+1-Rhs+1);

  mm_initialize_typecode(&matcode);
  mm_set_matrix(&matcode);
 
  if (VarType(1)==10){
    GetRhsVar(1,"c",&m1,&n1,&l1);
  }

  if (VarType(2)==5){

    GetRhsVar(2,"s",&m2,&n2,&A);

    mm_set_coordinate(&matcode);

    f=fopen(cstk(l1),"w");

    if(A.it==0){
      mm_set_real(&matcode);
      mm_set_general(&matcode);
      mm_write_banner(f,matcode);
      mm_write_mtx_crd_size(f,A.m,A.n,A.nel);
      for(row=0;row<A.m;row++){
	for(i=0;i<A.mnel[row];i++,index++){
	  fprintf(f,"%d %d %23.16le\n",row+1,A.icol[index],A.R[index]);
	}
      }
    } else if (A.it==1){
      mm_set_complex(&matcode);
      mm_set_general(&matcode);
      mm_write_banner(f,matcode);
      mm_write_mtx_crd_size(f,A.m,A.n,A.nel);
      for(row=0;row<A.m;row++){
	for(i=0;i<A.mnel[row];i++,index++){
	  fprintf(f,"%d %d %23.16le %23.16le\n",row+1,A.icol[index],A.R[index],A.I[index]);
	}
      }
    }
    fclose(f);
  } 
  else if (VarType(2)==1){

    GetRhsVar(2,"d",&m2,&n2,&l2);

    /* conversion du type de l'adresse l2 ( real ) en int par iadr */
    l=iadr(l2);
    /* recuperation de l'adresse de la case pr�c�dent celle qui contient les donn�es */
    it1=*istk(l-1);

    f=fopen(cstk(l1),"w");

    if ( it1==0 ){
      mm_set_array(&matcode);
      mm_set_real(&matcode);
      mm_set_general(&matcode);
      mm_write_banner(f,matcode);
      mm_write_mtx_array_size(f,m2,n2);

      for(i=0;i<n2*m2;i++) 
	fprintf(f,"%23.16le\n",stk(l2)[i]);

    } else if (it1==1){
      mm_set_array(&matcode);
      mm_set_complex(&matcode);
      mm_set_general(&matcode);
      mm_write_banner(f,matcode);
      mm_write_mtx_array_size(f,m2,n2);
      for(i=0;i<m2*n2;i++) 
	fprintf(f,"%23.16le %23.16le\n",stk(l2)[i],stk(l2)[i+m2*n2]);
    }

    fclose(f);

  } else {
    Scierror(503,"Second argument must be a matrix");
    return (0);
  }

  CreateVar(3,"i",&un,&un,&l3);

  *istk(l3)=err;

  LhsVar(1)=3;

  return(0);
}
